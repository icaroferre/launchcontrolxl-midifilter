"""
This is a setup.py script generated by py2applet

Usage:
    python setup.py py2app
"""

from setuptools import setup

APP = ['midi.py']
DATA_FILES = ['MainMenu.xib', 'Template.json']
OPTIONS = {'argv_emulation': False} 

setup(
    app=APP,
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    setup_requires=['py2app'],
    name="Filter",
    version="1.0.0",
)
