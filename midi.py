import mido
import json
from Cocoa import *
from Foundation import NSObject
import sys
import logging
import os

# Create a log file for the future debug
LOG_FILE_NAME = 'lcxl-filter.log'
logger = logging.getLogger('lcxl-filter')
log_path = path = os.path.join(os.path.expanduser('~'), '~/logs/')
if not os.path.exists(log_path):
    os.makedirs(log_path)
logger.setLevel(logging.DEBUG)
file_handler = logging.FileHandler(LOG_FILE_NAME)
file_handler.setLevel(logging.DEBUG)
file_formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s -%(message)s')
file_handler.setFormatter(file_formatter)
logger.addHandler(file_handler)

# Set Path for Template file
pathtoHere = os.path.dirname(os.path.realpath(__file__))
fileName = "/Template.json"
fullFilePath = pathtoHere + fileName

logger.info(fullFilePath)

# Read JSON File
with open(fullFilePath, 'r', encoding='utf-8') as templateFile:
    # Initialize Dictionaries
    template = {}
    selTemplate = {}
    # Read Json content
    template = json.load(templateFile)


def updateDict(n):
    global selTemplate  # Reach for Global Variable
    global Template
    selTemplate = {} #Initialize selTemplate
    n = str(n)  # Change Template number to String
    try:
        global Template
        global selTemplate  # Reach for Global Variable
        # Set selTemplate to the template dict key that corresponds to the
        # selected Template (n)
        selTemplate = template[n]
        logger.info("Raw selTemplate value: " + str(selTemplate))
        for key in selTemplate:
            if type(key) == str:
            	# LOG TO LOGFILE
                logger.info("-")
                logger.info("List of CC values defined as String")
                logger.info("Processing Key: " + key)
                logger.info("Key Original Value: " + selTemplate[key])
                selTemplate[key] = selTemplate[key].split(
                    " ")  # Split by space character
                # Log Valid Channels for Selected Template
                logger.info("Channel Number: " + key)
                # Transform strings to integers
                for n in range(len(selTemplate[key])):
                    selTemplate[key][n] = int(selTemplate[key][n])
                # Log Valid CC for Selected Template
                logger.info("Valid CC Numbers: " + str(selTemplate[key]))
            else:
                logger.info("List of CC values defined as Array.")
                # Log Valid Channels for Selected Template
                logger.info("Channel Number: " + key)
                # Log Valid CC for Selected Template
                logger.info("Valid CC Numbers: " + str(selTemplate[key]))
            # Format Text for JSON UI TextField
            textFieldFormat = ""
        for key in selTemplate:
            availableCC = ""
            for n in selTemplate[key]:
                availableCC = availableCC + str(n) + " "
            textFieldFormat = textFieldFormat + "Channel " + key + ": " + availableCC + "\n"
        viewController.updateJSON(str(textFieldFormat))  # Update UI
    except KeyError:
        logger.info("Channel not included in JSON file.")
        pass  # If Channel number is not in Template, pass.


def validateCC(CC, Value, Channel):
    strChannel = str(Channel + 1)  # Change Channel index (1-16)
    try:
        if CC in selTemplate[strChannel]:  # If CC number is in Template
            msg = mido.Message('control_change', control=CC,
                               value=Value, channel=Channel)  # Reformat Message
            outputPort.send(msg)  # Output message to LaunchControl XL
        else:
            logger.info("CC Not Matched")
            pass  # If CC number is not in Key, pass.
    except KeyError:
        logger.info("Channel Not Matched")
        pass  # If Channel number is not in Template, pass.

# Receive loop


def inMsg(message):
    logger.info(message)
    mChannel = message.channel
    mType = message.type
    logger.info(selTemplate)
    # Run validateCC function if incoming message is CC
    if mType == "control_change":
        mValue = message.value
        mCC = message.control
        validateCC(mCC, mValue, mChannel)

# Detect Launch Control XL Template Changes


def LCXLMsg(message):
    if message.type == "sysex":  # If Message is Sysex
        logger.info("Incoming Sysex Message: " + str(message))
        if message.data[5] == 119:  # Detect Layout Changes
            logger.info("New Template Selected")
            sysexTemp = message.data[6] + 1
            logger.info("Selected Template: " + str(sysexTemp))
            # User vs Factory Template
            if (sysexTemp < 9):
                viewController.updateDisplay_(
                    "Template Selected: User " + str(message.data[6] + 1))  # Update UI
            else:
                viewController.updateDisplay_(
                    "Template Selected: Factory " + str(message.data[6] + 1 - 8))  # Update UI
            updateDict(sysexTemp)  # Update Dictionary with Selected Template

# Set MIDI i/o ports
# For receiving Sysex Messages
LCXLinputPort = mido.open_input('Launch Control XL', callback=LCXLMsg)
inputPort = mido.open_input('Driver IAC LCXL', callback=inMsg)
outputPort = mido.open_output('Launch Control XL')


class LCXLFilter(NSWindowController):
    messageLabel = objc.IBOutlet()
    jsonLabel = objc.IBOutlet()

    # Update UI Label
    def updateDisplay_(self, message):
        self.message = message
        self.messageLabel.setStringValue_(self.message)

    def updateJSON(self, message):
        self.message = message
        self.jsonLabel.setStringValue_(self.message)


if __name__ == "__main__":
    app = NSApplication.sharedApplication()

    # Initiate the contrller with a XIB
    viewController = LCXLFilter.alloc().initWithWindowNibName_("MainMenu")

    # Show the window
    viewController.showWindow_(viewController)

    # Bring app to top
    NSApp.activateIgnoringOtherApps_(True)

    from PyObjCTools import AppHelper
    AppHelper.runEventLoop()

sys.exit(NSApplicationMain(sys.argv))
